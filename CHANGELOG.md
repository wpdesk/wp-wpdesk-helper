## [3.0.0] - 2021-09-01
### Removed
- license integration

## [2.4.2] - 2021-07-11
### Fixed
- Remove upgrade WC notice when no WC is active

## [2.4.1] - 2020-09-17
### Fixed
- PHP notice in remove_object_action_by_name

## [2.4.0] - 2020-08-12
### Added
- Notice about old PHP/WC/WP version

## [2.3.4] - 2020-01-22
### Fixed
- Cleaned translations

## [2.3.3] - 2020-01-22
### Fixed
- Cleaned translations

## [2.3.2] - 2019-11-20
### Fixed
- Menu destroyed by translations
### Changed
- Textdomain from wpdesk-helper to wpdesk-helper-textdomain

## [2.3.1] - 2019-11-14
### Fixed
- Typo in PL translation
- Url for deactivation&removal

## [2.3.0] - 2019-11-13
### Added
- Info about helper removal

## [2.2.0] - 2019-11-13
### Changed
- Removed .mo file
- Translation set in composer extra section

## [2.1.0] - 2019-11-11
### Added
- License 2.6

## [2.0.4] - 2019-10-11
### Changed
- Translations update

## [2.0.3] - 2019-08-17
### Changed
- Tracker interop
- Use filters to create tracker instance

## [2.0.1] - 2019-07-30
### Changed
- Tracker 2.0

## [2.0.0] - 2019-07-25
### Added
- Disables Helper 1.x and provides prefixed 2.x helper
- Action wpdesk_helper_initialized
- Filter wpdesk_prefixed_helper_is_loaded

## [1.3.3] - 2019-07-16
### Fixed
- Support for old FS with old wpdesk/log library - inject null falback logger

## [1.3.2] - 2019-07-03
### Fixed
- Support for old FS with old wpdesk/log library

## [1.3.1] - 2019-07-02
### Changed
- Minor change in translated phrase "Subskrypcje WP Desk"

## [1.3.0] - 2019-06-05
### Added
- Translation for subscriptions

## [1.2.3] - 2019-06-05
### Fixed
- Notice in LibraryDebug line 102

## [1.2.2] - 2019-05-31
### Fixed
- Warning in LibraryDebug line 34

## [1.2.1] - 2019-05-16
### Fixed
- Conflict of wp-notice and wp-logs

## [1.2.0] - 2019-05-06
### Added
- Info about used libraries in logs
- Info about used libraries in a new admin page wp-admin/admin.php?page=wpdesk-helper-library-report

## [1.1.3] - 2019-04-23
### Fixed
- Tests

## [1.1.2] - 2019-04-18
### Fixed
- Tracker translations

## [1.1.1] - 2019-04-18
### Fixed
- Inverted section order

## [1.1.0] - 2019-04-18
### Added
- Logger integration notice
- Null logger support

## [1.0.1] - 2019-04-17
### Fixed
- Namespaces

## [1.0.0] - 2019-04-17
### Added
- First version