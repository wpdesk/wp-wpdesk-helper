[![pipeline status](https://gitlab.com/wpdesk/wp-wpdesk-helper/badges/master/pipeline.svg)](https://gitlab.com/wpdesk/wp-wpdesk-helper/pipelines) 
[![coverage report](https://gitlab.com/wpdesk/wp-wpdesk-helper/badges/master/coverage.svg)](https://gitlab.com/wpdesk/wp-wpdesk-helper/commits/master) 
[![Latest Stable Version](https://poser.pugx.org/wpdesk/wp-wpdesk-helper/v/stable)](https://packagist.org/packages/wpdesk/wp-wpdesk-helper) 
[![Total Downloads](https://poser.pugx.org/wpdesk/wp-wpdesk-helper/downloads)](https://packagist.org/packages/wpdesk/wp-wpdesk-helper) 
[![Latest Unstable Version](https://poser.pugx.org/wpdesk/wp-wpdesk-helper/v/unstable)](https://packagist.org/packages/wpdesk/wp-wpdesk-helper) 
[![License](https://poser.pugx.org/wpdesk/wp-wpdesk-helper/license)](https://packagist.org/packages/wpdesk/wp-wpdesk-helper)

WordPress Library to track data.
===================================================
